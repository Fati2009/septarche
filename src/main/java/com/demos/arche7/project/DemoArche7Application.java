package com.demos.arche7.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoArche7Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoArche7Application.class, args);
	}

}
