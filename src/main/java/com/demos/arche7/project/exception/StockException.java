package com.demos.arche7.project.exception;

public class StockException extends Exception{
    public StockException (String message) {
        super (message);
    }
}
